package com.miaosha.dao;

import com.miaosha.domain.Goods;
import com.miaosha.domain.MiaoshaGoods;
import com.miaosha.domain.MiaoshaUser;
import com.miaosha.domain.User;
import com.miaosha.vo.GoodsVo;
import org.apache.ibatis.annotations.*;

import java.util.List;
//只要对应就行不用管是不是一个表对应的实体类
@Mapper
public interface GoodsDao {

    @Select("select g.*,mg.stock_count, mg.start_date, mg.end_date,mg.miaosha_price from miaosha_goods mg left join goods g on mg.goods_id = g.id")
    public List<GoodsVo> listGoodsVo();

    @Select("select g.*,mg.stock_count, mg.start_date, mg.end_date,mg.miaosha_price from miaosha_goods mg left join goods g on mg.goods_id = g.id where g.id = #{goodsId}")
    public GoodsVo getGoodsVoByGoodsId(@Param("goodsId")long goodsId);

    @Update("update miaosha_goods set stock_count = stock_count - 1 where goods_id = #{goodsId} and stock_count > 0")
    public int reduceStock(MiaoshaGoods g);

    @Update("update miaosha_goods set stock_count = #{stockCount} where goods_id = #{goodsId}")
    public int resetStock(MiaoshaGoods g);

}

