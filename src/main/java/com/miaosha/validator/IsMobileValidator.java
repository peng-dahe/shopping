package com.miaosha.validator;

import com.miaosha.util.ValidatorUtil;
import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintValidatorContextImpl;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

public class IsMobileValidator implements ConstraintValidator<IsMobile,String> {

    private boolean require=false;

    @Override
    public void initialize(IsMobile isMobile) {
        require=isMobile.required();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(require){
            return ValidatorUtil.isMobile(s);
        }
        else{
            if(StringUtils.isEmpty(s)){
                return true;
            }
            else {
                return ValidatorUtil.isMobile(s);
            }
        }
    }
}
