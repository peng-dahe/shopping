package com.miaosha.controller;

import com.miaosha.domain.User;
//import com.miaosha.rabbitmq.MQSender;
import com.miaosha.redis.RedisService;
import com.miaosha.redis.UserKey;
import com.miaosha.result.Result;
import com.miaosha.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Queue;

@Controller
@RequestMapping("/demo")
public class SampleController {

    @Autowired
    private UserService userService;
    @Autowired
    private RedisService redisService;

//    @Autowired
////    MQSender mqSender;
//
//    @RequestMapping("/mq")
//    @ResponseBody
//    public Result<String> dbGet(){
//        mqSender.send("hello,rabbitmq");
//        return Result.success("hello word");
//    }

    @RequestMapping("/redis/get")
    @ResponseBody
    public Result<User> redisGet(){
        return Result.success(redisService.get(UserKey.getById,""+1,User.class));
    }

    @RequestMapping("/redis/set")
    @ResponseBody
    public Result<Boolean> redisSet(){
        User user=new User(987654,"54188");
        Boolean ref=redisService.set(UserKey.getById,""+1,user);
        return Result.success(true);
    }

    @RequestMapping("/thymeleaf")
    public String thymeleaf(Model model){
        model.addAttribute("name","Joshua");
        return "hello";
    }


}
