package com.miaosha.access;

import com.miaosha.domain.MiaoshaUser;

public class UserContext {
    //ThreadLocal每个线程里面单独一份与当前线程绑定
    public static ThreadLocal<MiaoshaUser>  userHolder=new ThreadLocal<MiaoshaUser>();

    public static void setUser(MiaoshaUser user){
        userHolder.set(user);
    }

    public static MiaoshaUser getUser() {
        return userHolder.get();
    }
}
