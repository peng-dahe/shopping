package com.miaosha.vo;

import com.miaosha.domain.Goods;
import com.miaosha.service.GoodsService;

import java.math.BigDecimal;
import java.util.Date;

public class GoodsVo extends Goods {
    private Integer stockCount;

    private Date startDate;

    private Date endDate;
    private BigDecimal miaoshaPrice;

    public BigDecimal getMiaoshaPrice() {
        return miaoshaPrice;
    }

    public void setMiaoshaPrice(BigDecimal miaoshaPrice) {
        this.miaoshaPrice = miaoshaPrice;
    }

    public GoodsVo() {
    }

    public GoodsVo(Long id, String goodsName, String goodsTitle, String goodsImg, BigDecimal goodsPrice,
                   Integer goodsStock, String goodsDetail, Integer stockCount, Date startDate, Date endDate,
                   BigDecimal miaoshaPrice) {
        super(id, goodsName, goodsTitle, goodsImg, goodsPrice, goodsStock, goodsDetail);
        this.stockCount = stockCount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.miaoshaPrice = miaoshaPrice;
    }

    public Integer getStockCount() {
        return stockCount;
    }

    public void setStockCount(Integer stockCount) {
        this.stockCount = stockCount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
