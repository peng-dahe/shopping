package com.miaosha.vo;

import com.miaosha.domain.MiaoshaUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsDetailVo {

    private GoodsVo goods;
    private int miaoshaStatus=0;
    private int remainSeconds=0;
    MiaoshaUser user;

}
