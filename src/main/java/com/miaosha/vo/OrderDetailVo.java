package com.miaosha.vo;

import com.miaosha.domain.OrderInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderDetailVo {

    private GoodsVo goods;
    private OrderInfo order;

}
