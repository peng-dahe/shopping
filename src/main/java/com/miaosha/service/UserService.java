package com.miaosha.service;

import com.miaosha.dao.UserDao;
import com.miaosha.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    public User getById(Integer id){
        return userDao.getBId(id);
    }

}
